#! /usr/bin/env sh

sudo apt install zsh -y
chsh -s /usr/bin/zsh
sudo apt install git wget
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
source ~/.zshrc

